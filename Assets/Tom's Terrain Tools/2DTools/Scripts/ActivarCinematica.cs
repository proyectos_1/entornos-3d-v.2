using UnityEngine;
using Cinemachine;

public class ActivarCinematica : MonoBehaviour
{
    public CinemachineVirtualCamera camaraCinematica;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Jugador")
        {
            camaraCinematica.Priority = 20;
        }
    }
}
