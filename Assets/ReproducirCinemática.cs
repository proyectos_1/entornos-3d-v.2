using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;

public class ReproducirCinemática : MonoBehaviour
{
    public CinemachineVirtualCamera camaraCinemática;
    public PlayableDirector timelineCinemática;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            camaraCinemática.Priority = 11;
            timelineCinemática.Play();
        }
    }
}

